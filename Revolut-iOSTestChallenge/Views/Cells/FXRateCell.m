//
//  FXRateCell.m
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/5/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import "FXRateCell.h"

@implementation FXRateCell

- (void)setViewModel:(FXRateViewModel *)viewModel {
    _viewModel = viewModel;
    self.currency1NameLabel.text = NSLocalizedString(@"1 EUR", "Default currency value in EUR");
    self.currency2NameLabel.text = viewModel.currencyNameText;
    self.currency2RateLabel.text = viewModel.currencyValueText;
}
@end
