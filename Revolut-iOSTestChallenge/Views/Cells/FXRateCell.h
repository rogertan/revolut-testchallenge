//
//  FXRateCell.h
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/5/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXRateViewModel.h"

@interface FXRateCell : UITableViewCell

@property (nonatomic, copy) FXRateViewModel *viewModel;
@property (strong, nonatomic) IBOutlet UILabel *currency1NameLabel;
@property (strong, nonatomic) IBOutlet UILabel *currency2NameLabel;
@property (strong, nonatomic) IBOutlet UILabel *currency2RateLabel;

@end
