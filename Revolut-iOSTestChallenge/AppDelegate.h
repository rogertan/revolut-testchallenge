//
//  AppDelegate.h
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/2/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

