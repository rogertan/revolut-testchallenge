//
//  FXRate.h
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/5/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  FXRate Model class
 */
@interface FXRate : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *currency;
@property (nonatomic, assign) double rate;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
