//
//  FXRate.m
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/5/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import "FXRate.h"

NSString *const kFXRateCurrency = @"currency";
NSString *const kFXRateRate = @"rate";

@interface FXRate ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation FXRate

@synthesize currency = _currency;
@synthesize rate = _rate;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.currency = [self objectOrNilForKey:kFXRateCurrency fromDictionary:dict];
            self.rate = [[self objectOrNilForKey:kFXRateRate fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.currency forKey:kFXRateCurrency];
    [mutableDict setValue:[NSNumber numberWithDouble:self.rate] forKey:kFXRateRate];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.currency = [aDecoder decodeObjectForKey:kFXRateCurrency];
    self.rate = [aDecoder decodeDoubleForKey:kFXRateRate];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_currency forKey:kFXRateCurrency];
    [aCoder encodeDouble:_rate forKey:kFXRateRate];
}

- (id)copyWithZone:(NSZone *)zone
{
    FXRate *copy = [[FXRate alloc] init];
    
    if (copy) {

        copy.currency = [self.currency copyWithZone:zone];
        copy.rate = self.rate;
    }
    
    return copy;
}


@end
