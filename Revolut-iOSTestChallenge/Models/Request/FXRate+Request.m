//
//  FXRate+Request.m
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/5/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import "FXRate+Request.h"
#import "EuropaDotEuAPIClient.h"
#import "EuropaDotEuDailyFXParser.h"

@implementation FXRate (Request)

+ (void)fetchFXRatesInBackgroundWithCompletion:(FXRatesRequestCompletion)completion {
    
    EuropaDotEuAPIClient *client = [EuropaDotEuAPIClient sharedClient];
    
    [client GET:@"stats/eurofxref/eurofxref-daily.xml"
     parameters:nil
       progress:nil
        success:^(NSURLSessionDataTask * __unused task, id response) {
            if ([response isKindOfClass:[NSXMLParser class]]) {
                [[EuropaDotEuDailyFXParser sharedParser] convertNSXMLParser:response
                                                                 completion:^(NSArray *fxquotes, NSError *error)
                {
                     if (error == nil) {
                         NSMutableArray *fxRatesMutable = [[NSMutableArray alloc] init];
                         for (id obj in fxquotes) {
                             if ([obj isKindOfClass:[NSDictionary class]]) {
                                 [fxRatesMutable addObject:[[FXRate alloc] initWithDictionary:obj]];
                             }
                         }
                         completion([NSArray arrayWithArray:fxRatesMutable], nil);
                     } else {
                         completion(nil, error);
                     }
                 }];
            }
        }
        failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            completion(nil, error);
        }];
}

@end
