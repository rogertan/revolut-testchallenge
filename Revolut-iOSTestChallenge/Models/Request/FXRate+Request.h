//
//  FXRate+Request.h
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/5/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import "FXRate.h"

typedef void (^FXRatesRequestCompletion)(NSArray *rates, NSError *error);

@interface FXRate (Request)

/**
 *  Fetch FXRates in background
 *
 *  @param completion NSArray of FXRate if succeded, error if failed
 */
+ (void)fetchFXRatesInBackgroundWithCompletion:(FXRatesRequestCompletion)completion;

@end
