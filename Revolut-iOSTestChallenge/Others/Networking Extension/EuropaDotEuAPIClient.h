//
//  EuropaDotEuApiClient.h
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/2/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

/**
 *  A subclass of AFHTTPSessionManager which is configured to the API of Europa.eu
 */
@interface EuropaDotEuAPIClient : AFHTTPSessionManager

/**
 *  Singleton of EuropaDotEuAPIClient
 *
 *  @return an instance of EuropaDotEuAPIClient
 */
+ (instancetype)sharedClient;

@end
