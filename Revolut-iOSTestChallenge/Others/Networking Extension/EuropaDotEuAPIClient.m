//
//  EuropaDotEuApiClient.m
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/2/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import "EuropaDotEuApiClient.h"

static NSString * const EuropaDotEuAPIClientBaseURLString = @"http://www.ecb.europa.eu/";

@implementation EuropaDotEuAPIClient

+ (instancetype)sharedClient {
    static EuropaDotEuAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[EuropaDotEuAPIClient alloc] initWithBaseURL:[NSURL URLWithString:EuropaDotEuAPIClientBaseURLString]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        _sharedClient.responseSerializer = [AFXMLParserResponseSerializer serializer];
    });
    
    return _sharedClient;
}

@end
