//
//  EuropaDotEuDailyFXParser.m
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/2/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import "EuropaDotEuDailyFXParser.h"
#import "DataModels.h"


@interface EuropaDotEuDailyFXParser () <NSXMLParserDelegate>

@property (nonatomic, strong) NSDictionary *lastResult;
@property (nonatomic) FXCompletion completion;
@property (nonatomic, strong) NSMutableArray *fxQuotes;

@end

@implementation EuropaDotEuDailyFXParser


+ (instancetype)sharedParser {
    static EuropaDotEuDailyFXParser *_sharedParser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedParser = [[EuropaDotEuDailyFXParser alloc] init];

    });
    
    return _sharedParser;
}

- (void)convertNSXMLParser:(NSXMLParser *)xmlParser
                completion:(FXCompletion)completion {
    // Initialization
    _fxQuotes = [[NSMutableArray alloc] init];
    // Setup
    _completion = completion;
    xmlParser.delegate = self;

    if ([xmlParser parse] == NO) {
        
    }
}

#pragma mark - NSXMLParserDelegate

- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    if (attributeDict != nil && [attributeDict objectForKey:@"currency"] && [attributeDict objectForKey:@"rate"]) {
        [_fxQuotes addObject:attributeDict];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    if (_completion != nil) {
        _completion([NSArray arrayWithArray:_fxQuotes], nil);
    }
}

@end