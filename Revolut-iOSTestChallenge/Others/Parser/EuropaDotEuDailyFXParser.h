//
//  EuropaDotEuDailyFXParser.h
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/2/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EuropaDotEuDailyFXParser : NSObject

typedef void (^FXCompletion)(NSArray *fxquotes, NSError *error);

/**
 *  Singleton of EuropaDotEuDailyFXParser
 *
 *  @return an isntance of EuropaDotEuDailyFXParser
 */
+ (instancetype)sharedParser;

/**
 *  Converts the NSXMLParser into NSDictionnary Value
 *
 *  @param xmlParser  xmlParser response from 
 *  @param completion return an array of dictionnary rates if succeded or an error if failed
 */
- (void)convertNSXMLParser:(NSXMLParser *)xmlParser
                completion:(FXCompletion)completion;

@end
