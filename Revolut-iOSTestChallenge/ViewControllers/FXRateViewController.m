//
//  FXRateViewController.m
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/3/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

// View Controllers
#import "FXRateViewController.h"
// View Models
#import "FXRateViewModel.h"
// Views
#import "FXRateCell.h"

@interface FXRateViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSArray *fxRateViewModels;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *statusBarButtonItem;

@end

@implementation FXRateViewController

#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(fetchFXRateViewModels) userInfo:nil repeats:YES];
    [_timer fire];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [_timer invalidate];
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.fxRateViewModels == nil) {
        return 0;
    } else {
        return self.fxRateViewModels.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FXRateCell"];
    if (cell == nil) {
        cell = [[FXRateCell alloc] init];
    }
    if ([cell isKindOfClass:[FXRateCell class]]) {
        FXRateCell *rateCell = (FXRateCell *)cell;
        rateCell.viewModel = self.fxRateViewModels[indexPath.row];
    }
    
    return cell;
}

#pragma mark - Table View Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0;
}

#pragma mark - IBActions

- (IBAction)addCurrencyAction:(UIBarButtonItem *)sender {
    // To implements
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Information", @"Generic title for Alert Controller")
                                                                             message:NSLocalizedString(@"This feature will be available soon", @"A message for a feature not developped")
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK")
                                                       style:UIAlertActionStyleCancel
                                                     handler:nil]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - Privates

/**
 *  Fetches the rate view model
 */
- (void)fetchFXRateViewModels {
    [FXRateViewModel fetchFXRatesInBackground:^(NSArray *fxRates, NSError *error) {
        if (error == nil) {
            self.fxRateViewModels = fxRates;
            [self updateLastUpdate];
            [self.tableView reloadData];
        } else {
            NSLog(@"Error in FXRateViewController : %@", error.localizedDescription);
        }
    }];
}

/**
 *  Updates the last update in statusBarButtonItem
 */
- (void)updateLastUpdate {
    NSString *timeText = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                          dateStyle:NSDateFormatterNoStyle
                                                          timeStyle:NSDateFormatterMediumStyle];
    self.statusBarButtonItem.title = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Last Update at", "Last update text"), timeText];
}


@end
