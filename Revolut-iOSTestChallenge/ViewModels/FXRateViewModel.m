//
//  FXRateViewModel.m
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/3/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import "FXRateViewModel.h"
#import "FXRate+Request.h"

@interface FXRateViewModel ()

@property (nonatomic, readwrite) FXRate *fxRate;

@end

@implementation FXRateViewModel

- (instancetype)initWithFXRate:(FXRate *)fxRate {
    if (self = [super init]) {
        self.fxRate = fxRate;
    }
    
    return self;
}

+ (void)fetchFXRatesInBackground:(FXRatesViewModelCompletion)completion {
    [FXRate fetchFXRatesInBackgroundWithCompletion:^(NSArray *rates, NSError *error) {
        if (error == nil) {
            NSMutableArray *fxRatesMutable = [[NSMutableArray alloc] init];
            for (id rate in rates) {
                if ([rate isKindOfClass:[FXRate class]]) {
                    [fxRatesMutable addObject:[[FXRateViewModel alloc] initWithFXRate:rate]];
                }
            }
            completion([NSArray arrayWithArray:fxRatesMutable], nil);
        } else {
            completion(nil, error);
        }

    }];
}

- (double)currencyValueRaw {
    return self.fxRate.rate;
}

- (NSString *)currencyNameText {
    return self.fxRate.currency;
}

- (NSString *)currencyValueText {
   return [NSString stringWithFormat:@"%f", self.fxRate.rate];
}
@end
