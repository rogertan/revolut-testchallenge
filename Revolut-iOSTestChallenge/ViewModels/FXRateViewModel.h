//
//  FXRateViewModel.h
//  Revolut-iOSTestChallenge
//
//  Created by Roger TAN on 8/3/16.
//  Copyright © 2016 Roger TAN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FXRate+Request.h"

/**
 *  FXRateViewModel class
 */
@interface FXRateViewModel : NSObject

typedef void (^FXRatesViewModelCompletion)(NSArray *fxRates, NSError *error);

/**
 *  Gets the currency name
 */
@property (nonatomic, readonly) NSString *currencyNameText;
/**
 *  Gets a string value of the currency rate
 */
@property (nonatomic, readonly) NSString *currencyValueText;
/**
 *  Gets a raw value of currency rate
 */
@property (nonatomic, readonly) double currencyValueRaw;

/**
 *  Initializes the viewModel with FXRate Model
 *
 *  @param fxRate FXRate model to set
 *
 *  @return FXRateViewModel Return an instance of ViewModel
 */
- (instancetype)initWithFXRate:(FXRate *)fxRate;

/**
 *  Fetches the lastest exchange rate
 *
 *  @param completion Return an array of FXRateViewModel if the request was succedded or an error if the request failed
 */
+ (void)fetchFXRatesInBackground:(FXRatesViewModelCompletion)completion;

@end
