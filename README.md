# README #

This is the iOS Challenge of Revolut.

### How do I get set up? ###

First, please install Cocoapods in your Mac.

When Cocoapods is installed, please run the following command line `pod install`

After all dependencies are installed. Please open the file `Revolut-iOSTestChallenge.xcworkspace` and build the project.

Run the project and voilà!